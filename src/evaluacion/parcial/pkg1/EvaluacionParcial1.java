/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluacion.parcial.pkg1;

import java.io.IOException;

/**
 *
 * @author edgardopanchana
 */
public class EvaluacionParcial1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Cola cola = new Cola();
        Pila pila = new Pila();
        //Elementos en la Pila:
        System.out.println("Elementos en la Pila");
        pila.Imprimir();

        System.out.println("Elementos en la Pila despues de quitar un elemento:");
        pila.Pop();
        pila.Imprimir();

        System.out.println("Elementos en la COLA");
        cola.Imprimir();

        System.out.println("Elementos en la COLA despues de quitar un elemento:");
        cola.Avanzar();
        cola.Imprimir();

        System.out.println("datos de las estructuras **************************");
        cola.Imprimir();
        pila.Imprimir();
        cola.compararPila(pila);

        //Recuerde que para llamar a cada método lo debe hacer a través del objeto
        //Ejemplo: Si quiere llamar al método Pop de la Pila la sentencia sería pila.Pop();
        //Ejemplo: Si quiere llamar al método Avanzar de la Cola la sentencia sería cola.Avanzar();
        //Desarrolle un método retorne Verdadero si el dato que sale (avanza) de la Cola está en la Pila,
        //caso contrario retorne Falso.
        //Muestre el resultado por consola --> System.out.println();
    }

}
